
function createMessage (status, message, data = []) {
  var msg = {
    status: status,
    message: message,
    data: data
  }
  return msg
}

module.exports = {
  createMessage
}
