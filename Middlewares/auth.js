var services = require('../Controllers/service')
var messages = require('../Messages/msgs')

function isAuth (req, res, next) {
  var url = req.url
  if (url === '/signUp' || url === '/signIn') {
    next()
  } else {
    if (!req.headers.authorization) {
      var msg = messages.createMessage(403, 'La peticion no tiene cabecera')
      res.send({ msg })
    } else {
      var token = req.headers.authorization
      services.decodeToken(token)
        .then(response => {
          next()
        })
        .catch(error => {
          res.send({ error })
        })
    }
  }
}

module.exports = isAuth
