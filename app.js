var express = require('express')
var bodyParser = require('body-parser')
var cors = require('cors')
var app = express()
var api = require('./routes')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
app.use('/api', api)
//exportamos app
module.exports = app
