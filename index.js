var app = require('./app')
var config = require('./config')
var connection = require('./Connection/connection')

app.listen(config.PORT, (req, res) => {
  console.log(`La API se esta ejecutando en http://localhost:${config.PORT}`)
})
app.get('/conectarsePool',(req,res)=>{
    connection.connectPool()
})
