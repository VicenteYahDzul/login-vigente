
var connection = require('../Connection/connection')

function select (sql) {
  var cn = connection.toConnect()
  var promise = new Promise((resolve, reject) => {
    cn.query(sql, (err, result, fields) => {
      if (err) {
        throw err
        reject(err)
      } else {
        resolve(result)
      }
    })
    cn.end()
  })
  return promise
}

function insert (sql) {
  var cn = connection.toConnect()
  var promise = new Promise((resolve, reject) => {
    cn.query(sql, (err, result, fields) => {
      if (err) {
        throw err
        reject(err)
      } else {
        resolve(result)
      }
    })
    cn.end()
  })
  return promise
}

module.exports = {
  select,
  insert
}
