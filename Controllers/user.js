
var SqlString = require('sqlstring')
var service = require('./service')
var queries = require('./queries')
var messages = require('../Messages/msgs')

function signIn (user) {
  var sql = `SELECT * FROM users WHERE email = ${SqlString.escape(user.email)}`
  var promise = new Promise((resolve, reject) => {
    queries.select(sql)
      .then(response => {
        var password = user.password
        if (response.length === 0 || service.comparePasswords(password, response[0].password) === false) {
          var msg = messages.createMessage(403, 'Credenciales Invalidas')
          resolve(msg)
        } else {
          var token = service.createToken(response)
          msg = messages.createMessage(200, 'Acceso Correcto', token)
          resolve(msg)
        }
      })
      .catch(err => {
        throw err
        var msg = messages.createMessage(403, 'Credenciales Invalidas')
        reject(msg)
      })
  })
  return promise
}

function signUp (credentials) {
  var user = {
    name: credentials.name,
    email: credentials.email,
    password: service.encodePassword(credentials.password)
  }

  var sql = `INSERT INTO users SET name = ${SqlString.escape(user.name)},` +
  `email = ${SqlString.escape(user.email)},` +
  `password = ${SqlString.escape(user.password)}`
  var promise = new Promise((resolve, reject) => {
    var consult = `SELECT email FROM users WHERE email = ${SqlString.escape(user.email)}`
    queries.select(consult)
      .then(result => {
        if (result.length === 0) {
          queries.insert(sql)
            .then(response => {
              var msg = messages.createMessage(201, 'Registro Existoso', user)
              resolve(msg)
            })
            .catch(err => {
              throw err
              var msg = messages.createMessage(500, 'Error al registrar el usuario')
              reject(msg)
            })
        } else {
          var msg = messages.createMessage(403, 'Email asociado a cuenta existente')
          reject(msg)
        }
      })
      .catch(err => {
        throw err
        reject(err)
      })
  })
  return promise
}

function showAllUsers () {
  var sql = 'SELECT * FROM users'
  var promise = new Promise((resolve, reject) => {
    queries.select(sql)
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        throw err
        var msg = messages.createMessage(500, 'Error inesperado')
        reject(msg)
      })
  })
  return promise
}

module.exports = {
  signIn,
  signUp,
  showAllUsers
}
