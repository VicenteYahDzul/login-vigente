
var jwt = require('jwt-simple')
var moment = require('moment')
var bcrypt = require('bcrypt')
var config = require('../config')
var messages = require('../Messages/msgs')

function createToken (user) {
  var payload = {
    sub: user[0].id,
    iat: moment().unix(),
    exp: moment().add(1, 'minutes').unix(),
    email: user[0].email,
    password: user[0].password
  }
  return jwt.encode(payload, config.SECRET_TOKEN)
}

function decodeToken (token) {
  var mensaje
  var promise = new Promise((resolve, reject) => {
    try {
      var payload = jwt.decode(token, config.SECRET_TOKEN, config.isVerify)
    } catch (err) {
      var msg = messages.createMessage(401, 'Invalid token')
      reject(msg)
    }
    if (payload.exp <= moment().unix()) {
      var msg = messages.createMessage(403, 'El token ha expirado')
      reject(msg)
    } else {
      resolve(payload)
    }
  })
  return promise
}

function encodePassword (password) {
  var saltRounds = 10
  var hash = bcrypt.hashSync(password, saltRounds)
  return hash
}

function comparePasswords (password, hash) {
  var isSame = bcrypt.compareSync(password, hash)
  return isSame
}

module.exports = {
  createToken,
  decodeToken,
  encodePassword,
  comparePasswords
}
