'use strict'

var express = require('express')
var api = express.Router()
var UserCtrl = require('../Controllers/user')
var messages = require('../Messages/msgs')
var middleware = require('../Middlewares/auth')

api.use(middleware)

api.post('/signIn', (req, res) => {
  var user = req.body
  UserCtrl.signIn(user)
    .then(response => {
      res.send({ response })
    })
    .catch(err => {
      res.send({ err })
    })
})

api.get('/auth', (req, res) => {
  UserCtrl.showAllUsers()
    .then(result => {
      var msg = messages.createMessage(200, 'Autorizado')
      res.send({ msg, result })
    })
    .catch(err => {
      res.send({ err })
    })
})

api.post('/signUp', (req, res) => {
  var user = req.body
  UserCtrl.signUp(user)
    .then(response => {
      res.send({ response })
    })
    .catch(err => {
      res.send({ err })
    })
})

module.exports = api
