'use strict'

var pg = require('pg')
var config = require('../config')

function connectPool () {
  var pool = pg.createPool({
    connectionLimit: 10,
    host: config.host,
    port: config.port,
    user: config.user,
    password: config.password,
    database: config.database
  })

  pool.getConnection((err, connection) => {
    if (err) {
      throw err
    } else {
      console.log('conexion Exitosa')
    }
  })
  return pool
}

function Connect () {
  var connection = pg.createConnection({
    host: config.host,
    user: config.user,
    password: config.password,
    database: config.database
  })

  connection.connect((err) => {
    if (err) {
      console.log('error de conexion')
      throw err
      
    } else {
      console.log('conexion exitosa')
    }
  })
  return connection
}
module.exports = {
  toConnect,
  connectPool
}
